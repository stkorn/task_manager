import Storage from "@/helpers/storage/Storage"
import Cache, {keys as cacheKeys} from "@/helpers/cache/Cache"

export default {
    mounted() {
        const {$message} = this
        const {warning, error, success} = $message
        const {dispatch, commit} = this.$store

        window.addEventListener('offline', () => {
            commit('network/setOnline', false)
            error({message: 'Соединение с сетью потеряно...'})
        })

        window.addEventListener('online', () => {
            commit('network/setOnline', true)
            success({message: 'Соединение восстановлено'})

                if (this.$route.name === 'boardById') {
                    let creationTasks = Storage.get(cacheKeys.CREATE_TASKS) || []
                    let editionTasks = Storage.get(cacheKeys.UPDATE_TASKS) || []

                    editionTasks.map(task => {
                        const duplicate = creationTasks.find(({id}) => id === task.id)
                        if (duplicate)
                            creationTasks = creationTasks.filter(({id}) => id !== task.id)
                    })

                    const payload = {editionTasks, creationTasks}

                    dispatch('board/sync', {id: this.$route.params.id, payload}).then(async () => {
                        warning({message: 'Синхронизация с сервером...'})
                        await dispatch('board/getById', this.$route.params.id)
                        //Чистим кеш после обновления доски
                        for (let key in cacheKeys)
                            Cache.clearMemory(cacheKeys[key])
                        success({message: 'Синхронизация успешно завершена', duration: 5000})
                    })
                }
        })
    }
}
