export const EventSystem = {
    SWITCH_MODAL_CREATION_TASK: 'switchModalCreationTask',
    SWITCH_MODAL_EDITION_TASK: 'switchModalEditionTask',
    SWITCH_MODAL_CREATION_BOARD: 'switchModalCreationBoard',
    SWITCH_MODAL_EDITION_BOARD: 'switchModalEditionBoard',
    SWITCH_MODAL_INVITE_USER: 'switchModalInviteUser',
    SWITCH_MODAL_MANAGE_INVITATION: 'switchModalManageInvitation',
    BOARD_CLEAR_SEARCH: 'boardClearSearch',
    BOARD_REARRANGE: 'boardReArrange'
}
