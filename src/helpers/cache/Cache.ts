import Storage from "@/helpers/storage/Storage"

export const keys = {
    UPDATE_TASKS: 'ut',
    CREATE_TASKS: 'ct',
    //...modules keys
}

type TCacheOptions = {
    key: string
}

export default class Cache {

    protected key: string

    constructor(public options: TCacheOptions) {
        const {key} = options
        if (key)
            this.key = key
        else throw new Error('Cache key required')
    }

    static clearMemory(key) {
        Storage.remove(key)
    }
}
