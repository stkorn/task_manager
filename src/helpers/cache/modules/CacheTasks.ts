import Storage from "@/helpers/storage/Storage"
import Cache from "@/helpers/cache/Cache"

export class CacheTasks extends Cache {
    constructor(options) {
        super(options)
    }

    saveTask(id, newTask) {
        const key = this.key

        if (Storage.isKey(key)) {
            const tasks = Storage.get(key)
            const i = tasks.findIndex(task => task.id === id)

            !!tasks.filter(task => task.id === id).length ?
                tasks[i] = newTask :
                tasks.push(newTask)

            Storage.set(key, tasks)
        }
        else Storage.set(key, [{...newTask}])
    }
}
