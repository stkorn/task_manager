export default class Storage {

    static set(key: string, value: any): void {
        localStorage.setItem(key, JSON.stringify(value))
    }

    static get(key: string): any {
        return JSON.parse(localStorage.getItem(key))
    }

    static isKey(key: string): boolean {
        return !!localStorage.getItem(key)
    }

    static remove(key: string): void {
        localStorage.removeItem(key)
    }
}
