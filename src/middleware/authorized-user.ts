import store from '@/store'

export default {
    beforeEnter: function (to, from, next) {
        if (!store.getters['user/auth'])
            next({name: 'index'})
        else next()
    }
}
