import store from '@/store'

export default {
    beforeEnter: function (to, from, next) {
        if (!store.getters['user/auth']) {
            // store.$app.$message.warning({message: `Недосточно прав для перехода в раздел ${to.path}. Авторизируйтесь в системе.`, showClose: true})
            next({name: 'index'})
        }
        else next()
    }
}


