import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import Cookie from "@/helpers/cookie/Cookie"
import Account from './routes/accout'

import middleware from "@/middleware/middleware"

Vue.use(VueRouter)

const routes = [
    ...Account,
    {
        path: '/',
        name: 'index',
        component: () => import('@/pages/index.vue'),
    },
    {
        path: '/auth/login',
        name: 'login',
        component: () => import('@/pages/auth/login/index.vue'),
        beforeEnter: (to, from, next) => {
            if (store.getters['user/authenticated'])
                next('/')
            else next()
        }
    },
    {
        path: '/auth/signup',
        name: 'signup',
        component: () => import('@/pages/auth/signup/index.vue'),
        beforeEnter: (to, from, next) => {
            if (store.getters['user/authenticated'])
                next('/')
            else next()
        }
    },
    {
        path: '/auth/password_restore',
        name: 'passwordRestore',
        component: () => import('@/components/Auth/PasswordRestore.vue'),
        beforeEnter: (to, from, next) => {
            if (store.getters['user/authenticated'])
                next('/')
            else next()
        }
    },
    {
        path: '/boards',
        name: 'boards',
        component: () => import('@/pages/boards/index.vue'),
        ...middleware
    },
    {
        path: '/board/:id',
        name: 'boardById',
        component: () => import('@/pages/board/_id/index.vue'),
        ...middleware
    },
    // {
    //     path: '/board/:id/revision',
    //     name: 'boardRevision',
    //     component: () => import('@/pages/board/_id/revision/index.vue'),
    //     // ...middleware
    // },
    {
        path: '/404',
        name: '404NotFound',
        component: () => import('@/components/HttpStatus/404NotFound.vue')
    },
    {path: '*', redirect: '/404'},
]

const router = new VueRouter({mode: 'history', base: process.env.BASE_URL, routes})

router.beforeEach((to, from, next) => {
    const token = Cookie.get('token')
    const {commit} = store
    token ? commit('user/auth', true) : commit('user/auth', false)
    token ? commit('user/setToken', token) : commit('user/setToken', null)
    next()
})

export default router
