import authorizedUser from "@/middleware/authorized-user"
export default [
    {
        path: '/account/settings',
        name: 'settings',
        component: () => import('@/pages/account/settings/index.vue'),
        ...authorizedUser
    }
]
