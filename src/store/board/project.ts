import {Project} from "@/types/project"
import {Task} from "@/types/task"
import ProjectService from "@/services/ProjectService";

export default {
    namespaced: true,
    state: {
        projects: [] as Project[],
    },
    actions: {},
    mutations: {
        setProjects: (state, payload: Project[]) => state.projects = payload,
        addTask(state, task: Task) {
            const project: Project = state.projects.find(project => project.title === task.project)

            if (!project) {
                state.projects.push(<Project>{title: task.project, tasks: [task]})
            }
            else {
                project.tasks.push(task)
            }
        }
    },
    getters: {
        getProjects: state => <Project[]>state.projects,
        getTitles: state => <Project[]>state.titles
    }
}
