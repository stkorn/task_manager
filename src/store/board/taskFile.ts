import TaskFileService from "@/services/TaskFileService"
import {Task, TaskFile} from "@/types/task"

const namespaced = true

const state = {
    files: [] as TaskFile[]
}

const actions = {
    async uploadAll(ctx, {taskId, files}) {
        try {
            await TaskFileService.uploadAll(taskId, files)
        } catch (e) {console.log(e)}
    },
    async upload(ctx, {taskId, file}) {
        try {
            const task: Task = await TaskFileService.upload(taskId, file)
            const files = task.files
            ctx.commit('setFiles', files)
        } catch (e) {console.log(e)}
    },
    async delete(ctx, {taskId, fileId}) {
        try {
            await TaskFileService.delete(taskId, fileId)
            ctx.commit('delete', fileId)
        } catch (e) {console.log(e)}
    }
}

const mutations = {
    add: (state, payload) => state.files.push(payload),
    update: (state, payload) => state.files.forEach(file => file.id === payload.id ? file = payload : null),
    delete: (state, id) => state.files = state.files.filter(file => file.id !== id),
    setFiles: (state, payload) => state.files = payload
}

const getters = {
    getFiles: state => state.files
}

export default {namespaced, state, actions, mutations, getters}
