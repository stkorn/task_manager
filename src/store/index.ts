import Vue from 'vue'
import Vuex from 'vuex'

import user from "./user"
import board from "./board/board"
import viewport from "./viewport"
import network from "@/store/network"
import tab from "./board/tab"
import task from "./board/task"
import taskFile from "@/store/board/taskFile"
import project from "./board/project"
import subtask from "./board/subtask"
import comment from "./board/comment"
import executor from "./board/executor"

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
      user,
      board,
      viewport,
      network,
      tab,
      task,
      taskFile,
      project,
      subtask,
      comment,
      executor
  }
})

export const {dispatch, commit, getters} = store
export default store
