import api from "@/helpers/http/api"
import Cookie from "@/helpers/cookie/Cookie"
import {convertServerResponse} from "@/helpers/utils"

export default {
    namespaced: true,
    state: () => ({
        auth: false,
        user: {},
        role: null
    }),
    actions: {
        async signup(ctx, user) {
            try {
                const res = await api.post('/register/', user, {headers: {'Authorization': ''}})
                return res.data
            } catch (e) {return e.response.data}
        },
        async login(ctx, user) {
            try {
                const res = await api.post('/signin/', user, {headers: {'Authorization': ''}})
                if (res.status === 200) {
                    const {token, user, expires_in} = res.data
                    ctx.commit('setUser', user)
                    ctx.commit('setToken', token)
                    const lifetime = new Date(Date.now() + Math.floor(expires_in * 1000)).toUTCString()
                    Cookie.set('token', token, {expires: lifetime})
                }
                return res.data
            } catch (e) {
                Cookie.removeAll()
                ctx.commit('setToken', null)
                return e.response.data
            }
        },
        async logout() {
            await Cookie.removeAll()
            location.replace('/')
        },
        async saveUser(ctx) {
            const res = await api.get('/profile/')
            const user = await res.data
            ctx.commit('setUser', user)
        },
        async updateUser(ctx, user) {
            try {
                const res = await api.put('/profile/', user)
                ctx.commit('setUser', res.data)
                return convertServerResponse(res)
            } catch (e) {return convertServerResponse(e.response.data)}
        },
        async refreshPassword(ctx, {password, password_confirm}) {
            try {
                let res = await api.post('/password-refresh/', {password, password_confirm})
                return convertServerResponse(res)
            } catch (e) {return convertServerResponse(e.response)}
        },
    },
    mutations: {
        auth: (state, payload) => state.auth = payload,
        setToken: (state, payload) => state.token = payload,
        setUser: (state, payload) => state.user = payload,
        setRole: (state, payload) => state.role = payload
    },
    getters: {
        auth: state => state.auth,
        getToken: state => state.token,
        authenticated: state => !!state.token,
        getUser: state => state.user,
        getRole: state => state.role
    },
}
