import {Task} from "@/types/task"

export type Project = {
    readonly id: number
    title: string
    tasks?: Task[]
}
