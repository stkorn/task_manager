import {Project} from "@/types/project"

export type Task = {
    readonly id: number,
    title: string,
    status: TaskStatus,
    files?: TaskFile[],
    comments?: Comment[]
    subtasks?: Array<any>
    icon: string | null
    responsible?: TaskExecutor
    executors?: Array<any>
    project?: string
    projectId?: number,
    projects?: Project[]
}

export type TaskExecutor = {
    id: number | string
    fullName: string
}

export type TaskHistory = {
    readonly id: number,
    action?: string,
    changed?: string
    user?: string
}

export type TaskIcon = {
    readonly id: number
    description?: string,
    image?: string
}

export type Comment = {
    readonly id?: number
    text?: string,
    date?: string
}

export type TaskFile = {
    readonly id: string | number
    name: string
    task?: number
    file?: string
}

export type TaskStatus = 'В работе' | 'Требуется помощь' | 'Отменено' | 'Завершено' | 'Завершено и подтверждено' | 'Просрочено'
export type TaskMark = 'Возвращено на доработку' | 'Подтверждено'
