const path = require('path')

module.exports = {
    css: {
        loaderOptions: {
            sass: {
                additionalData: `@import "@/ui/_index.scss";`
            }
        },
    },
    pwa: {
        // workboxPluginMode: 'InjectManifest',
        // workboxOptions: {
        //     swSrc: path.join(__dirname, 'src', 'sw.js')
        // }
    },
}
